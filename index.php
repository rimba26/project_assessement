<?php

include 'database.php';
include 'function.php';


$siswa=$db->query("select * from penjualan");
$data_siswa=$siswa->fetchAll();
// echo $data_siswa;

if(isset($_GET['delete'])){
  deleteSiswa($_GET);
  header('location:index.php');
}

if(isset($_POST['simpan'])){
  inputData($_POST);
  header('location: index.php');
}



?>

<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
           <link rel="shortcut icon" href="image/icon.png" type="image/x-icon">
            <title>Shofee</title>
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/all.css">
        </head>
        <body>
        <nav class="navbar"  style="background: linear-gradient(to left,#0000ff,#b3ffff);">
  <a href="index.php"> <img src="img/logo1.png" alt="logo" width="300px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  </div>
</nav>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4" id="slidebar">
                    <div class="d-flex flex-column pt-5 col-11">
                    <a href="index.php" style="text-decoration: none;"><h4><i class="fas fa-home"></i>Home</h4></a>
                            </div>
                        <nav class="nav flex-column" style="font-size: large;">
                          <a class="nav-link active" href="tambahdata.php">Tambah Data</a>
                          <a class="nav-link" href="tampil.php">Edit Data</a>
                          <a class="nav-link" href="daftar_product.php">Lihat Data</a>
                          <a class="nav-link" href="tutorial.html">Tutorial</a>
                         </nav>

     
                    
                        <div class="d-flex flex-row mt-5">
                            <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="button" aria-pressed="false"><a href="About.html" style="text-decoration:none;color:white"> About me</a></button>
                            
                         </div>
                        <!-- footer -->
                        <div class="d-flex flex-row" style="color: #3399ff;">
                            Copyright &copy; Muhammad_Rimba
                        </div>
                    </div>

                    <!-- vector -->
                    <div class="col-8 vh-100 border pt-5" id="vector">
                    <div class="card text-white bg-warning" style="max-width: 18rem; float:right;">
  <div class="card-header">Edit Data</div>
  <div class="card-body">
    <h5 class="card-title">Edit Data</h5>
    <p class="card-text">Klil tombol dibawah untuk mengedit data anda.</p>
    <button class="btn btn-primary"><a href="tampil.php" style="text-decoration: none;color:white">Edit</button>
  
  </div>
</div>
<div class="card text-white bg-success mb-3 pt-4" style="max-width: 18rem;">
  <div class="card-header">Tambah Data</div>
  <div class="card-body">
    <h5 class="card-title">Tambah Data</h5>
    <p class="card-text">Menu ini akan membawa anda ke halaman tambah data.</p>
    <button class="btn btn-primary"><a href="tambahdata.php" style="text-decoration: none;color:white">Create</button>
  </div>
</div>
<div class="card text-white bg-primary mb-3 mt-5" style="float:left; max-width:18rem">
  <div class="card-header">Tutorial</div>
  <div class="card-body">
    <h5 class="card-title">Tutorial</h5>
    <p class="card-text">Klik tombol di bwah jika anda membutuhkan tutorial atau bantuan untuk mengisi form, jika ada masalah silahkan hubungi kami.</p>
    <button class="btn btn-light"><a href="tutorial.html" style="text-decoration: none;color:#0066ff">Tutorial</a></button>
  </div>
</div>
<div class="card text-white bg-dark mb-3 mt-5" style="max-width: 18rem; float:right">
  <div class="card-header">About</div>
  <div class="card-body">
    <h5 class="card-title">About me</h5>
    <p class="card-text">Jika anda perlu bantuan bisa menghubungi kami dengan klik tombol dibawah untuk info komunikasi.</p>
    <button class="btn btn-light"><a href="About.html" style="text-decoration: none;color:#404040">About me</button>
  </div>
</div>
</div>
</div>
</div>
  

            
            <script src="js/jquery-3.5.1.slim.min.js"></script>
            <script src="js/popper.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
        </body>
        </html>