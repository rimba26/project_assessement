<?php

include 'database.php';
 include 'function.php';
 include 'connection.php';

 $tampung=[];
 if(isset($_POST['cari']))
 {
  $tampung=searching($_POST['search']);
}

 if(!empty($tampung))
 {
   
    $data_daftar=$tampung;
 }


 
//  if(isset($_GET['delete'])){
//   deleteSiswa($_GET);
//   header('location:tampil.php');
// }

//  if(isset($_POST['search']))
// {


//     $filter=$db->quote($_POST['search']);
    

//     $name=$_POST['search'];

//     $search=$db->prepare("select * from penjualan where nama_penjual=? or nama_barang=? or harga=?");

//     $search->bindValue(1,$name,PDO::PARAM_STR);
//     $search->bindValue(2,$name,PDO::PARAM_STR);
//     $search->bindValue(3,$name,PDO::PARAM_STR);

//     $search->execute();

//     $tampil_data=$search->fetchAll(); 

//     $row = $search->rowCount();

//   }else{
//     $data = $db->query("select * from penjualan ");
    

//     $tampil_data = $data->fetchAll();
// }


    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <title>Table Penjualan</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a href="index.php"> <img src="img/logo1.png" alt="logo" width="300px"></a>
  <form class="form-inline" action="tampil.php" method="POST">
    <div class="form-group mx-sm-3 mb-2">
        <input type="text" class="form-control" name="search" placeholder="harga atau barang">
        <input class="btn btn-success" type="submit" value="cari" name="cari" style="margin-left: 10px;">
    </div>
</form>

</nav>
<?php if(isset($row)):?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <p class="lead "><?php echo $row;?> Telah Terdeteksi !</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>


<!-- Allert Massage -->

<table class="table table-bordered table-primary ">
  <thead>
    <tr>
      <th scope="col">Id Poduk</th>
      <th scope="col">Nama Seller</th>
      <th scope="col">Barang</th>
      <th scope="col">Harga</th>
      <th scope="col">Gambar</th>
      <th scope="col">Fitur</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach ($data_daftar as $key): ?>
    <tr action="tampil.php" method="POST">
      <td><?php echo $key['nomor_seri'];?></td>
      <td><?php echo $key['nama_penjual'];?></td>
      <td><?php echo $key['nama_barang'];?></td>
      <td><?php echo $key['harga'];?></td>
      <td ><img src="uploads/<?php echo $key['foto'];?>" width="200px" height="200px"></td>
      <td> <a class="btn btn-danger" href="delete.php?delete=&nomor_seri=<?php echo $key['nomor_seri']?>"onclick="return confirm('Apakah anda yakin untuk menghapus data ini?')">Delete</a> | <a class="btn btn-success" href="edit.php?nomor_seri=<?php echo $key['nomor_seri']; ?>">Edit</a></td>
    </tr>
      <?php endforeach; ?>
  </tbody>
</table>


<div class="pt-2" style="background: linear-gradient(to right,#1a1a1a,#cccccc); margin-top:345px">
<p style="color: white; text-align:center">&copy; Shofee by Muhammad Rimba Abdillah <?php echo date('Y');?></p>
      </div>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>