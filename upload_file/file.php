<?php


/** 
 *  Ambil koneksi kedalam database
 *  dicontoh ini tabelnya 
 * 
 *  create table file(id int not null primary key auto_increment,path varchar(255)); 
 */
include '../connection.php';

if (isset($_POST['action'])) {
    $upload = uploadFiles($_FILES, $db->lastInsertId());

    /**  
     * Jika returnnya string berarti ditemukan error
     * jika array tinggal querykan kedalam database
     */

    if (is_array($upload)) {

        $save_path = $db->prepare('insert into penjualan(foto) values(?)');

        $save_path->bindParam(1, $upload[1], PDO::PARAM_STR);
        // $save_path->bindParam(2, $_POST['caption'], PDO::PARAM_STR);

        if ($save_path->execute()) {
            $message = $upload[0];
        }
    } else {
        $message = $upload;
    }
}

function uploadFiles($files)
{
    /** 
     * Siapkan folder untuk upload file
     * $user_file variabel yang nantinya disimpan ke dalam database
     */

    $target_dir = "file/";
    $user_file = $target_dir . basename($files['fileUpload']['name']);

    /**  
     * Validasi file yang dikirim oleh user
     * - file hanya boleh gambar dengan format jpg,jpeg dan png
     * - file tidak boleh diatas 1mb
     * 
     *  jika sesuai dengan validasi siapkan nama untuk gambar yang akan
     *  diupload
     */

    $fileType = strtolower(pathinfo($user_file, PATHINFO_EXTENSION));

    $allowType = ['jpg', 'jpeg', 'png'];

    if (!in_array($fileType, $allowType)) {
        return "File not allowed";
    }

    $check = getimagesize($files["fileUpload"]["tmp_name"]);

    if (!$check) {
        return "File is not image";
    }

    if ($files["fileUpload"]["size"] > 1000000) {
        return "File is larger than 1mb";
    }

    $temp_name = $target_dir . 'upload_' . date('d-m-Y_His') . "." . $fileType;

    /** 
     *  jika bisa lolos semua validasi upload file dan simpan lokasi path gambar
     */

    if (!move_uploaded_file($files["fileUpload"]["tmp_name"], $temp_name)) {

        return "File not uploaded";
    }

    /** 
     * return lokasi gambar yang akan disimpan kedalam database
     */

    return ['file success uploaded', $temp_name];
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Upload Image</title>
</head>

<body>

    <div class="container">
        <div class="row mt-4">
            <div class="col-lg-4">
                <a class="btn btn-outline-primary" href="index.php">Home</a>
                <h4>Upload Image</h4>
                <?php if (isset($message)) : ?>
                    <div class="alert alert-primary" role="alert">
                       <?php echo $message;?>
                    </div>
                <?php endif; ?>
                <form action="file.php" method="post" enctype="multipart/form-data">
                    <textarea class="form-control" type="text" name="caption" placeholder="Caption" required></textarea>
                    <input class="form-control" type="file" name="fileUpload" required>
                    <input class="form-control mt-4" type="submit" value="Upload" name="action">
                </form>
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

</body>

</html>