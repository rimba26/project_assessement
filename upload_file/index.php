<?php

include '../connection.php';

$file = $db->prepare('select * from project1');

$file->execute();

$file_image = $file->fetchAll();

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>List Image</title>

    <style>
        img {
            width: 200px;
            /* You can set the dimensions to whatever you want */
            height: 200px;
            object-fit: cover;
        }
    </style>
</head>

<body>

    <div class="container-fluid p-0">
        <div class="row">
            <div class="col">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#">SampleGram</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="btn btn-primary" href="file.php">Upload</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <?php foreach ($file_image as $image) : ?>
                <div class="col-lg-3 p-0">
                    <div class="card">
                        <input type="hidden" value="">
                        <img src="<?php echo $image['path']; ?>" class="card-img-top">
                        <p class="lead card-caption"><?php echo $image['caption']; ?></p>
                        <div class="card-body text-center">
                            <a href="#" class="btn btn-primary modalPop">View</a>
                            <a href="#" class="btn btn-primary modalPop">Change</a>
                            <a href="#" class="btn btn-primary modalPop">Delete</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalPop" tabindex="-1" aria-labelledby="modalPop" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script> -->

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <script>
        $('.modalPop').click(function(el) {
            el.preventDefault();
            $('.modal-title').html($(this).html());
            action = $(this).html();

            if (action == 'View') {
                $('#modalPop').modal('show');
                img_src = $(this).parent().siblings('img').attr('src');
                html_img = "<img src=" + img_src + " style='width:100%;height:100%;'>";
                $('.modal-body').html(html_img);
            } else if (action == 'Change') {
                $('.modal-body').html("");
                $('#modalPop').modal('show');

                template=`
                    <form action="index.php" method="POST">
                        <input type="hidden" name="id">
                        <textarea class="form-control" name="caption"></textarea>
                        <input class="form-control" type="submit" name="action" value="Update">
                    </form>
                `;

                // $('.modal-body').html(template);
            }
        });
    </script>
</body>

</html>