<?php
include 'database.php';
include 'connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Daftar Product</title>
</head>
<body>
  
<nav class="navbar"  style="background: linear-gradient(to left,#0000ff,#b3ffff);">
  <a href="index.php"> <img src="img/logo1.png" alt="logo" width="300px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  </div>
</nav>
    <div class="container">
        <div class="row">
          <?php foreach ($data_daftar as $key): ?>
            <div class="col-lg-4">
            <div class="card "  style="height: 380px; margin-top:5%">
                  <div class="card-body">
                  <img class="card-img-top" src="uploads/<?php echo $key['foto'] ?>" alt="..." width="100px"; height="150px";>
                    <h5 class="card-title"><?php echo $key['nama_barang'];?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?php echo "Celler: " .$key['nama_penjual'];?></h6>
                    <p class="card-text">Data ini adalah data penjualan melalui aplikasi Shofee.</p>
                    <h6 ><?php echo "Rp. ".number_format($key['harga']); ?></h6>
                  </div>
            </div>
              
            </div>
            <?php endforeach; ?>
        </div>
    </div>




<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

</body>
</html>