<?php
 include 'database.php';
 include 'function.php';
 include 'connection.php';

//  if(isset($_POST['submit']))
//  {
//     //  inputData($_POST);
//     var_dump($_POST); die;
//     //  header('location:tampil.php');
//  }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Tambah Data</title>
</head>
<body>
<nav class="navbar"  style="background: linear-gradient(to left,#0000ff,#b3ffff);">
  <a href="index.php"> <img src="img/logo1.png" alt="logo" width="300px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  </div>
</nav>

<div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <form action="" method="POST" enctype="multipart/form-data">
                <div class="card mx-auto bg-dark" style="width: 30rem; color:white">
                    <div class="card-body">
                        <h2 style="text-align:center">Tambah Data</h2>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Seller</label>
                                    <input type="text" name="nama_penjual" class="form-control">
                                </div>
            
                                <div class="form-group">
                                    <label for="exampleInputEmail1">nama produk</label>
                                    <input type="text" name="nama_barang" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">harga</label>
                                    <input type="text" name="harga" class="form-control">
                                </div>
                                <div>
                                <div>
                                    <label for="exampleInputEmail1">Gambar</label>
                                    <br>
                                    <input type="file" name="gambar">
                                </div>
                                
                                <button type="submit" class="btn btn-success mt-3" name="kirim">simpan</button>
                                <button type="submit"  class="btn btn-danger mt-3" name="kirim"><a href="index.php" style="text-decoration: none; color:white">Batal</a></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <?php
        if(isset($_POST['kirim']))
        {
            $nama = $_POST['nama_penjual'];
            $jenis_produk = $_POST['nama_barang'];
            $nama_file = $_FILES['gambar']['name'];
            $source = $_FILES['gambar']['tmp_name'];
            $folder = './uploads/';
            $harga = $_POST['harga'];

            move_uploaded_file($source,$folder.$nama_file);
            $insert = mysqli_query($conn, "INSERT INTO penjualan VALUES (
                NULL,
                '$nama',
                '$jenis_produk',
                '$harga',
                '$nama_file')");

            // if($insert)
            // {
            //     echo "Berhasil Upload";
            // }else{
            //     echo "gagal upload";
            // }

            header('location:tampil.php');
        }


    ?>


<div class="pt-3" style="background: linear-gradient(to right,#1a1a1a,#cccccc); margin-top:88px">
<p style="color: white; text-align:center">&copy; Shofee by Muhammad Rimba Abdillah <?php echo date('Y');?></p>
      </div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

</body>
</html>
